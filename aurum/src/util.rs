use std::env::var_os;
use std::ffi::OsStr;
use std::fmt::{self, Formatter, Display};
use std::fs::metadata;
use std::path::{Path, PathBuf};
use std::process::Command as StdCommand;
use std::result;

use failure::{Error, err_msg, Fail};

pub type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub struct MissingPackage {
    pub pkgname: String
}

impl Display for MissingPackage {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "package not found: {}", self.pkgname)
    }
}

impl Fail for MissingPackage {}
unsafe impl Sync for MissingPackage {}
unsafe impl Send for MissingPackage {}

/// Wrapper over `process::Command` for doing appropriate output
pub struct Command(StdCommand);

impl Command {
    pub fn new(program: impl AsRef<OsStr>) -> Command {
        Command(StdCommand::new(program))
    }
    
    pub fn arg(&mut self, arg: impl AsRef<OsStr>) -> &mut Command {
        self.0.arg(arg);
        self
    }
    
    pub fn args(&mut self, args: impl IntoIterator<Item = impl AsRef<OsStr>>) -> &mut Command {
        self.0.args(args);
        self
    }
    
    pub fn env(&mut self, key: impl AsRef<OsStr>, val: impl AsRef<OsStr>) -> &mut Command {
        self.0.env(key, val);
        self
    }
    
    pub fn current_dir(&mut self, dir: impl AsRef<Path>) -> &mut Command {
        self.0.current_dir(dir);
        self
    }
    
    /// Returns an error if the status isn't success
    pub fn run(&mut self) -> Result<()> {
        info!("running {:?}", self.0);
        let status = self.0.status()?;
        
        if status.success() {
            Ok(())
        } else {
            let msg = err_msg(format!("{:?} exited {}", self.0, status));
            Err(msg)
        }
    }
}

#[derive(Clone, Debug, Eq, Hash)]
pub enum InstallReason {
    Explicit,
    Dependency,
}

impl PartialEq for InstallReason {
    /// This always returns true, since the install reason
    /// doesn't matter when comparing ActionType::Install.
    fn eq(&self, _other: &Self) -> bool {
        true
    }
}

/// Couple of helper funcs around executing a pacman bin
/// Includes `sudo`
pub struct Pacman;

impl Pacman {
    pub fn sync_pkgs(pkgs: &[impl AsRef<OsStr>]) -> Result<()> {
        self::Command::new("sudo")
            .arg("pacman")
            .arg("-S")
            .args(pkgs)
            // If this option should not have been passed, aurum is doing too much.
            //   Packages installed from the repos are only dependencies
            .arg("--asdeps")
            .arg("--noconfirm")
            .run()
    }
    
    pub fn install_pkgs(pkgs: &[impl AsRef<OsStr>], reason: InstallReason) -> Result<()> {
        // This is as close to the ugliest code I've written that I can remember
        // Suggestions welcome
        let mut cmd = self::Command::new("sudo");
        let mut cmd = cmd.arg("pacman")
            .arg("-U")
            .args(pkgs);
        
        match reason {
            InstallReason::Dependency => cmd = cmd.arg("--asdeps"),
            _ => {}
        }
        
        cmd.arg("--noconfirm")
            .run()
    }
}

/// If `path` exists, pass it back, otherwise, none
pub(crate) fn exists_path(path: PathBuf) -> Option<PathBuf> {
    if metadata(&path).is_err() {
        None
    } else {
        Some(path)
    }
}

/// Takes an environment variable key, and reads that variable. If
/// the variable has contents, that is returned, otherwise, `suffix`
/// is appended to the user's home directory.
//TODO: Make this pretty
pub(crate) fn xdg_dir(xdg_var: impl AsRef<OsStr>, suffix: impl AsRef<Path>) -> PathBuf {
    if let Some(path) = var_os(xdg_var) {
        let mut result = PathBuf::new();
        result.push(path);
        result
    } else {
        // Problem: Assumes $HOME is set
        let result = var_os("HOME").expect("$HOME is not set!");
        let mut result = PathBuf::from(result);
        result.push(suffix);
        result
    }
}
