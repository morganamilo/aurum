use std::cell::RefCell;
use std::fmt::Debug;

use failure::Error;
use raur::Handle;
use raur_ext::{Cache, Package};
use raur_ext::RaurExt;

use util::{MissingPackage, Result};

pub struct PkgInfoCache {
    cache: RefCell<Cache>,
    aur_handle: Handle,
}

impl PkgInfoCache {
    /// Significantly more efficient alternative to PkgInfoCache::get
    /// for populating the cache.
    pub fn add(&self, pkgs: &[impl AsRef<str> + Debug]) -> Result<()> {
        debug!("getting package info for {:?}", pkgs);
        
        self.aur_handle.cache_info(&mut self.cache.borrow_mut(), pkgs)?;
        Ok(()) // Mismatched error types, so this is a little uglier than it could be
    }
    
    /// This will get package info out of this cache, or download it from the
    /// aur if the reqest is a cache miss. Note that this clones out of the cache.
    pub fn get(&self, pkgname: impl AsRef<str>) -> Result<Package> {
        let mut cache = self.cache.borrow_mut();
        
        // I'd really rather it only spit out a message when missing the cache,
        //   but there's not an efficient way to do that without being inside raur-ext
        // This is basically assuming `add` misses more often than not
        //debug!("getting package info for {}", pkgname.as_ref());
        self.aur_handle.cache_info(&mut cache, &[pkgname.as_ref()])?;
        
        if let Some(pkg) = cache.get(pkgname.as_ref()) {
            Ok(pkg.clone())
        } else {
            Err(Error::from(MissingPackage { pkgname: pkgname.as_ref().to_string() }))
        }
    }
}

impl Default for PkgInfoCache {
    fn default() -> PkgInfoCache {
        PkgInfoCache {
            cache: RefCell::new(Cache::default()),
            aur_handle: Handle::default(),
        }
    }
}
