use std::rc::Rc;

use itertools::Itertools;
use pkg_utils::{Alpm, Provide};

use config::Config;
use self::Package::*;
use util::Result;

/// Represents a package that could have come from
/// a number of different locations
#[derive(Clone, Debug, PartialEq)]
pub enum Package {
    Aur(raur_ext::Package),
    Alpm(pkg_utils::package::MetaPackage),
}

impl Package {
    /// Determines the location of the package and passes
    /// back the appropriate variant
    // Don't check the local_db, the package origin matters, not the current location!
    pub fn new(pkgname: impl AsRef<str> + Ord, dbs: Rc<Alpm>, config: &Config) -> Result<Package> {
        let sync_pkg = dbs.sync_dbs.iter()
            .flat_map(|db| db.packages.iter() )
            .find(|pkg| &pkg.name == pkgname.as_ref() );
        
        if let Some(pkg) = sync_pkg {
            return Ok(Alpm(pkg.clone()));
        }
        
        Ok(Aur(config.cache.get(pkgname.as_ref().to_string())?))
    }
    
    pub fn name<'a>(&'a self) -> &'a str {
        match self {
            Aur(pkg) => &pkg.name,
            Alpm(pkg) => &pkg.name
        }
    }
    
    pub fn dependencies(&self) -> Option<Vec<String>> {
        let this = (*self).clone();
        match this {
            Aur(pkg) => {
                let deps = pkg.depends.iter().map(|vec| vec.clone() ).collect_vec();
                if deps.len() > 0 {
                    Some(deps)
                } else {
                    None
                }
            },
            Alpm(pkg) => Some(pkg.depends.clone())
        }
    }
    
    pub fn make_dependencies(&self) -> Option<Vec<String>> {
        let this = (*self).clone();
        match this {
            Aur(pkg) => {
                let deps = pkg.make_depends.iter().map(|vec| vec.clone() ).collect_vec();
                if deps.len() > 0 {
                    Some(deps)
                } else {
                    None
                }
            },
            Alpm(pkg) => Some(pkg.makedepends.clone())
        }
    }
    
    pub fn unresolved_depends(&self, dbs: Rc<Alpm>) -> impl Iterator<Item = String> {
        let deps = self.dependencies().unwrap_or(Vec::new());
        unresolved_deps(deps.into_iter(), dbs)
    }
    
    pub fn unresolved_makedepends(&self, dbs: Rc<Alpm>) -> impl Iterator<Item = String> {
        let deps = self.make_dependencies().unwrap_or(Vec::new());
        unresolved_deps(deps.into_iter(), dbs)
    }
}

fn unresolved_deps<'a, S, I>(deps: I, dbs: Rc<Alpm>) -> impl Iterator<Item = S>
    where S: AsRef<str> + Ord,
          I: Iterator<Item = S>
{
    deps.filter(move |pkgname| {
            let provide = Provide::parse(pkgname).expect("empty pkgname?");
            !dbs.local_db.provides(&provide)
        })
}
