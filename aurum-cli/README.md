# aurum-cli
CLI frontend for the [aurum](https://crates.io/crates/aurum) library, more info in the readme from that repo.

Running `cargo install aurum-cli` will install a binary called `aurum`. The aurum crate is just a library.
