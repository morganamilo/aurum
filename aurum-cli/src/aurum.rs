extern crate aurum;
#[macro_use]
extern crate clap;
extern crate colored;
extern crate fern;
extern crate itertools;
extern crate libc;
#[macro_use]
extern crate log;

use std::io::{stdin, stdout, Write};
use std::fmt::{Debug, Display};
use std::process::exit;

use clap::Values;
use colored::Colorize;
use itertools::Itertools;
use log::{Level, LevelFilter};

use aurum::*;
use aurum::ActionType::*;

//TODO: Push this into a new crate
trait OptionalExt {
    type Succ;
    
    fn unwrap_or_exit(self, exit_status: i32) -> Self::Succ;
}

impl<T, E: Debug + Display> OptionalExt for Result<T, E> {
    type Succ = T;
    
    fn unwrap_or_exit(self, exit_status: i32) -> T {
        self.unwrap_or_else(|err| {
            debug!("{:?}", err);
            error!("{}", err);
            exit(exit_status);
        })
    }
}

//*
fn getuid() -> usize {
    unsafe { libc::getuid() as usize }
}

fn main() {
    let matches = clap_app!(aurum =>
        (version: "0.1")
        (author: "Wesley Hershberger <mggmugginsmc@gmail.com>")
        (about: "A minimal AUR helper: See wiki for more in-depth help: https://gitlab.com/MggMuggins/aurum/wikis/home")
        (@arg verbose: -v --verbose ... "Set verbosity level")
        (@arg config: -c --config +takes_value "Set custom config file location")
        (@group targets =>
            (@attributes +required +multiple)
            (@arg TARGETS: ... "Packages to operate on")
            (@arg update: -u --update "Updates all AUR packages currently installed on the system")
        )
        (@group operation =>
            (@attributes +required)
            (@arg get: -G --get "Unpack TARGETS' PKGBUILDs and other files")
            (@arg build: -B --build "Build TARGETS with makepkg. Implies `fetch`")
            (@arg install: -I --install "Install TARGETS using pacman. Implies `build`")
        )
        
        //(@subcommand search =>
        //    (about: "Searches using given params and query string; shows package info")
        //)
    )
        .help(include_str!("../cli-help.md"))
        .get_matches();
    
    let log_level = match matches.occurrences_of("verbose") {
        0 => LevelFilter::Info,
        1 => LevelFilter::Debug,
        _ => LevelFilter::Trace
    };
    
    fern::Dispatch::new()
        .format(|out, message, record| {
            let prefix = match record.level() {
                Level::Error => "::".red().bold(),
                Level::Warn => "::".yellow().bold(),
                Level::Info => "::".blue().bold(),
                Level::Debug => "::".green().bold(),
                Level::Trace => "::".bold()
            };
            
            out.finish(format_args!(
                "{} {}",
                prefix,
                message
            ))
        })
        // I figure probably best that errors from other crates get through
        .level(LevelFilter::Error)
        //.level_for("pkg_utils", log_level)
        .level_for("aurum", log_level)
        .level_for("aurum_cli", log_level)
        .chain(std::io::stdout())
        .apply()
        .unwrap_or_exit(1);
    debug!("Logger init");
    
    if getuid() == 0 {
        error!("Do not run as root!");
        exit(1);
    }
    
    let mut aurum = Aurum::new(matches.value_of("config"))
        .unwrap_or_exit(1);
    
    let action = if matches.is_present("get") {
        Fetch
    } else if matches.is_present("build") {
        Build
    } else if matches.is_present("install") {
        Install(InstallReason::Explicit)
    } else {
        panic!("Missing an operation");
    };
    
    let targets = matches.values_of("TARGETS")
        .unwrap_or(Values::default());
    
    for pkgname in targets {
        let pkgname = pkgname.to_string();
        aurum.action((action.clone(), pkgname));
    }
    
    if matches.is_present("update") {
        aurum.action((Update(Box::new(action)), String::new()));
    }
    
    // Sets up the closure that will print all the actions that will be done before
    //   they are added, and ask the user to confirm.
    // This is a hot mess
    aurum.on_do_actions(|actions| {
        let (install_bins, mut fmtgroup): (Vec<(ActionType, &str)>, _) = actions
            .iter()
            .filter(|action| !action.0.is_update() )
            .unique() // Not sure why this isn't being done in aurum...
            .cloned() // Same as actions.clone().iter(), but more efficient
            .partition(|action| action.0 == InstallBin );
        
        let (install, mut fmtgroup): (Vec<(ActionType, &str)>, _) = fmtgroup
            .drain(..)
            // Note here that InstallReason can be anything because of the
            // `yes` impl of PartialEq on that type
            .partition(|action| action.0 == Install(InstallReason::Explicit) );
        
        let (build, mut fmtgroup): (Vec<(ActionType, &str)>, _) = fmtgroup
            .drain(..)
            .partition(|action| action.0 == Build );
        
        let (fetch, _): (Vec<(ActionType, &str)>, _) = fmtgroup
            .drain(..)
            .partition(|action| action.0 == Fetch );
        
        let install_bin_list = install_bins.iter()
            .map(|action| &action.1 )
            .format(", ");
        let install_bin_list = format!("{}", install_bin_list);
        
        let install_list = install.iter()
            .map(|action| &action.1 )
            .format(", ");
        let install_list = format!("{}", install_list);
        
        let build_list = build.iter()
            .map(|action| &action.1 )
            .format(", ");
        let build_list = format!("{}", build_list);
        
        let fetch_list = fetch.iter()
            .map(|action| &action.1 )
            .format(", ");
        let fetch_list = format!("{}", fetch_list);
        
        println!();
        if &install_bin_list != "" {
            println!("{} {}", "Dependencies in repo:".bold(), install_bin_list);
        }
        if &fetch_list != "" {
            println!("{} {}", "Packages to get:".bold(), fetch_list);
        }
        if &build_list != "" {
            println!("{} {}", "Packages to build:".bold(), build_list);
        }
        if &install_list != "" {
            println!("{} {}", "Packages to install:".bold(), install_list);
        }
        // Not easy to use logger :/
        print!("\n{} {}", "::".blue().bold(), "Proceed? [Y/n]: ".bold());
        let _failed_flush = stdout().flush();
        
        let mut response = String::new();
        stdin().read_line(&mut response).expect("Failed to read reponse");
        response.truncate(1);
        
        response.len() != 0 && response.pop().unwrap().to_ascii_lowercase() == 'y'
    });
    
    aurum.commit().unwrap_or_exit(1);
}
