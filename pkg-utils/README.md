# Pkg-Utils
This crate implements a _subset_ of libalpm badly for use in aurum. I didn't want to have to deal with an API that wasn't really designed for use from Rust (nor for doing dependency resolution for packages not included in the Db system), so I wrote my own. So far it seems to work well.

This crate is a heavy WIP and the API is by no means fixed. I anybody wants to use/build on this crate I am more than happy to marge back changes/iterate on issues/that kind of thing. Just shoot me issues/emails/MRs.
